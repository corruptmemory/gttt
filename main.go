package main

import (
	_ "embed"
	"fmt"
	"runtime"

	"github.com/go-gl/glfw/v3.3/glfw"
	vk "gitlab.com/corruptmemory/go-vulkan"
	"gitlab.com/corruptmemory/gttt/graphics"
)

//go:generate glslc shaders/shader.vert -o shaders/vert.spv
//go:generate glslc shaders/shader.frag -o shaders/frag.spv

var (
	//go:embed shaders/vert.spv
	vert []byte
	//go:embed shaders/frag.spv
	frag []byte
)

func init() {
	// This is needed to arrange that main() runs on main thread.
	// See documentation for functions that are only allowed to be called from the main thread.
	runtime.LockOSThread()
}

func main() {
	// defer profile.Start(profile.TraceProfile, profile.ProfilePath(".")).Stop()
	err := glfw.Init()
	if err != nil {
		panic(err)
	}
	defer glfw.Terminate()
	if !glfw.VulkanSupported() {
		panic(fmt.Errorf("Vulkan doesn't seem supported"))
	}

	fmt.Println("Hello Vulkan!")
	vk.SetGetInstanceProcAddr(glfw.GetVulkanGetInstanceProcAddress())
	if err = vk.Init(); err != nil {
		panic(err)
	}
	gctx := graphics.NewGContext()

	err = gctx.CreateVulkanInstance("gttt")
	if err != nil {
		panic(err)
	}
	defer gctx.Destroy()

	physicalDevices, err := gctx.PhysicalDevices()
	if err != nil {
		panic(err)
	}
	for _, pd := range physicalDevices {
		fmt.Printf("Physical device: %#v\n", pd)
	}

	// piece, err := wfm.LoadWavefrontFile("./assets/models/X.obj")
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("Piece:")
	// piece.ToWriter(os.Stdout)

	glfw.WindowHint(glfw.ClientAPI, glfw.NoAPI)

	window, err := glfw.CreateWindow(640, 480, "Testing", nil, nil)
	if err != nil {
		panic(err)
	}

	surface, err := window.CreateWindowSurface(gctx.Instance, nil)
	if err != nil {
		panic(err)
	}

	window.SetFramebufferSizeCallback(func(w *glfw.Window, width, height int) {
		gctx.FramebufferResized = true
	})

	gctx.Surface = vk.RawPointerToSurface(surface)

	err = gctx.PickPhysicalDevice()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Got a workable physical device")

	err = gctx.CreateLogicalDevice()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created a logical device")

	details, err := gctx.GetSwapChainSupportDetails()
	if err != nil {
		panic(err)
	}
	fmt.Printf("details: %#v\n", details)

	err = gctx.CreateSwapChain(window)
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Got a swapchain!!")

	err = gctx.CreateSwapChainImageViews()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created swap chain image views!!")

	err = gctx.CreateRenderPass()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created render pass!")

	err = gctx.CreateGraphicsPipeline(vert, frag)
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created graphics pipeline!")

	err = gctx.CreateFramebuffers()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created framebuffers!")

	err = gctx.CreateCommandPool()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created command pool!")

	err = gctx.CreateVertexBuffer()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created vertex buffer!")

	err = gctx.CreateCommandBuffers()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created command buffers!")

	err = gctx.CreateSyncObjects()
	if err != nil {
		panic(err)
	}
	fmt.Println("Woot! Created semaphores!")

	for !window.ShouldClose() {
		glfw.PollEvents()
		err = gctx.DrawFrame(window, vert, frag)
		if err != nil {
			panic(err)
		}
	}

	err = gctx.DeviceWait()
	if err != nil {
		panic(err)
	}
}
