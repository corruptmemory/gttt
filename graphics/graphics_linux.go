package graphics

import (
	vk "gitlab.com/corruptmemory/go-vulkan"
)

func init() {
	extensions = append(extensions, vk.KhrXlibSurfaceExtensionName, vk.KhrXcbSurfaceExtensionName)
}
