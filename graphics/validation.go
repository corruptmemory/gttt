// +build !novalidation

package graphics

import (
	"fmt"

	vk "gitlab.com/corruptmemory/go-vulkan"
)

var (
	validationLayers = []string{
		"VK_LAYER_KHRONOS_validation",
	}
)

func checkVulkanValidationLayerSupport(vr vk.VulkanResource) (err error) {
	properties, err :=  vr.EnumerateInstanceLayerProperties()
	if err != nil {
		return
	}

	remaining := len(validationLayers)
	loop:
	for _, layerName := range validationLayers {
		if remaining == 0 {
			return
		}
		for _, layerProperties := range properties {
			if layerName == layerProperties.LayerName {
				remaining--
				continue loop
			}
		}
		return fmt.Errorf("error: could not find validation layer: '%s'", layerName)
	}
	return
}
