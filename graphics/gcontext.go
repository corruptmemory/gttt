package graphics

import (
	"errors"
	"fmt"
	"math"
	"unsafe"

	"github.com/go-gl/glfw/v3.3/glfw"
	lm "github.com/xlab/linmath"
	vk "gitlab.com/corruptmemory/go-vulkan"
)

type GContext struct {
	vr                       vk.VulkanResource
	Instance                 vk.Instance
	PhysicalDevice           vk.PhysicalDevice
	Device                   vk.Device
	GraphicsQueue            vk.Queue
	PresentQueue             vk.Queue
	Surface                  vk.Surface
	SwapChain                vk.Swapchain
	FramesInFlightCount      uint
	SwapChainImages          []vk.Image
	SwapChainImageFormat     vk.Format
	SwapChainExtent          vk.Extent2D
	SwapChainImageViews      []vk.ImageView
	PipelineLayout           vk.PipelineLayout
	GraphicsPipeline         vk.Pipeline
	RenderPass               vk.RenderPass
	SwapChainFramebuffers    []vk.Framebuffer
	CommandPool              vk.CommandPool
	CommandBuffers           []vk.CommandBuffer
	ImageAvailableSemaphores []vk.Semaphore
	RenderFinishedSemaphores []vk.Semaphore
	InFlightFences           []vk.Fence
	ImagesInFlight           []vk.Fence
	CurrentFrame             uint32
	FramebufferResized       bool
	VertexBuffer             vk.Buffer
	VertexBufferMemory       vk.DeviceMemory
}

type Vertex struct {
	pos lm.Vec2
	color lm.Vec3
}

var vertices = []Vertex{
    {lm.Vec2{0.0, -0.5}, lm.Vec3{1.0, 0.0, 0.0}},
    {lm.Vec2{0.5, 0.5}, lm.Vec3{0.0, 1.0, 0.0}},
    {lm.Vec2{-0.5, 0.5}, lm.Vec3{0.0, 0.0, 1.0}},
}


func getBindingDescription() vk.VertexInputBindingDescription {
	return vk.VertexInputBindingDescription{
		Binding: 0,
		Stride: uint32(unsafe.Sizeof(Vertex{})),
		InputRate: vk.VertexInputRateVertex,
	}
}

func getAttributeDescriptions() []vk.VertexInputAttributeDescription {
    return []vk.VertexInputAttributeDescription{
    	{
				Binding: 0,
				Location: 0,
				Format: vk.FormatR32g32Sfloat,
				Offset: uint32(unsafe.Offsetof(Vertex{}.pos)),
    	},
    	{
				Binding: 0,
				Location: 1,
				Format: vk.FormatR32g32b32Sfloat,
				Offset: uint32(unsafe.Offsetof(Vertex{}.color)),
    	},
    }
}

func NewGContext() *GContext {
	return &GContext{
		vr: vk.NewVuklanResource(vk.NewArenaMemory(128*1024)),
	}
}

func (g *GContext) CreateVulkanInstance(applicationName string) (err error) {
	fmt.Printf("extensions: %v\n", extensions)
	err = checkVulkanValidationLayerSupport(g.vr)
	if err != nil {
		return
	}
	err = checkVulkanExtensionSupport(g.vr)
	if err != nil {
		return err
	}

	// Application info
	applicationInfo := vk.ApplicationInfo{
		ApplicationName:    applicationName,
		ApplicationVersion: vk.MakeVersion(1, 0, 0),
		EngineName:         "go-vulkan",
		ApiVersion:         ApiVersion12,
	}

	// Instance info
	instanceCreateInfo := vk.InstanceCreateInfo{
		ApplicationInfo:       &applicationInfo,
		EnabledLayerNames:     validationLayers,
		EnabledExtensionNames: extensions,
	}

	instance, err := g.vr.CreateInstance(&instanceCreateInfo)
	if err != nil {
		return err
	}
	g.Instance = instance
	return
}

type SwapChainSupportDetails struct {
	Capabilities vk.SurfaceCapabilities
	Formats      []vk.SurfaceFormat
	PresentModes []vk.PresentMode
}

func (s *SwapChainSupportDetails) ChooseSwapSurfaceFormat() vk.SurfaceFormat {
	for _, e := range s.Formats {
		if e.Format == vk.FormatB8g8r8a8Srgb && e.ColorSpace == vk.ColorSpaceSrgbNonlinear {
			return e
		}
	}
	return s.Formats[0]
}

func (s *SwapChainSupportDetails) ChooseSwapPresentMode() vk.PresentMode {
	for _, e := range s.PresentModes {
		if e == vk.PresentModeMailbox {
			return e
		}
	}
	return vk.PresentModeFifo
}

func maxUint32(a uint32, b uint32) uint32 {
	if a < b {
		return b
	}
	return a
}

func minUint32(a uint32, b uint32) uint32 {
	if a < b {
		return a
	}
	return b
}

func (s *SwapChainSupportDetails) ChooseSwapExtent(window *glfw.Window) vk.Extent2D {
	if s.Capabilities.CurrentExtent.Width != math.MaxUint32 {
		return s.Capabilities.CurrentExtent
	}
	w, h := window.GetFramebufferSize()
	actualExtent := vk.Extent2D{
		Width:  uint32(w),
		Height: uint32(h),
	}
	actualExtent.Width = maxUint32(s.Capabilities.MinImageExtent.Width, minUint32(s.Capabilities.MaxImageExtent.Width, actualExtent.Width))
	actualExtent.Height = maxUint32(s.Capabilities.MinImageExtent.Height, minUint32(s.Capabilities.MaxImageExtent.Height, actualExtent.Height))
	return actualExtent
}

var (
	ErrorNoPhysicalDevices     = errors.New("error: no physical devices with Vulkan support available")
	ErrorNoUsableQueueFamilies = errors.New("error: no usable queue families")
)

// CleanupSwapChain ...
func (g *GContext) CleanupSwapChain() error {
	if g.SwapChainFramebuffers != nil {
		for i, f := range g.SwapChainFramebuffers {
			g.vr.DestroyFramebuffer(g.Device, f)
			g.SwapChainFramebuffers[i] = nil
		}
	}
	g.SwapChainFramebuffers = nil

	if g.CommandBuffers != nil && g.CommandPool != nil {
		g.vr.FreeCommandBuffers(g.Device, g.CommandPool, g.CommandBuffers)
		g.CommandBuffers = nil
	}

	if g.GraphicsPipeline != nil {
		g.vr.DestroyPipeline(g.Device, g.GraphicsPipeline)
		g.GraphicsPipeline = nil
	}

	if g.PipelineLayout != nil {
		g.vr.DestroyPipelineLayout(g.Device, g.PipelineLayout)
		g.PipelineLayout = nil
	}

	if g.RenderPass != nil {
		g.vr.DestroyRenderPass(g.Device, g.RenderPass)
		g.RenderPass = nil
	}

	for i, iv := range g.SwapChainImageViews {
		g.vr.DestroyImageView(g.Device, iv)
		g.SwapChainImageViews[i] = nil
	}
	g.SwapChainImageViews = nil

	if g.SwapChain != nil {
		g.vr.DestroySwapchain(g.Device, g.SwapChain)
		g.SwapChain = nil
	}

	return nil
}

// RecreateSwapChain ...
func (g *GContext) RecreateSwapChain(window *glfw.Window,
	vert []byte,
	frag []byte) error {

	w, h := window.GetFramebufferSize()
	for w == 0 || h == 0 {
		glfw.PollEvents()
		w, h = window.GetFramebufferSize()
	}

	err := g.vr.DeviceWaitIdle(g.Device)
	if err != nil {
		return err
	}

	err = g.CleanupSwapChain()
	if err != nil {
		fmt.Printf("Cleanup swapshain didn't go accorording to plan: %v\n", err)
		return err
	}

	err = g.CreateSwapChain(window)
	if err != nil {
		fmt.Printf("Creating swapshain didn't go accorording to plan: %v\n", err)
		return err
	}

	err = g.CreateSwapChainImageViews()
	if err != nil {
		fmt.Printf("Creating swapshain image views didn't go accorording to plan: %v\n", err)
		return err
	}

	err = g.CreateRenderPass()
	if err != nil {
		fmt.Printf("Creating render pass didn't go accorording to plan: %v\n", err)
		return err
	}

	err = g.CreateGraphicsPipeline(vert, frag)
	if err != nil {
		fmt.Printf("Creating graphics pipeline didn't go accorording to plan: %v\n", err)
		return err
	}

	err = g.CreateFramebuffers()
	if err != nil {
		fmt.Printf("Creating framebuffers didn't go accorording to plan: %v\n", err)
		return err
	}

	err = g.CreateCommandBuffers()
	if err != nil {
		fmt.Printf("Creating command buffers didn't go accorording to plan: %v\n", err)
		return err
	}

	return nil
}

func (g *GContext) Destroy() {
	g.CleanupSwapChain()

	g.vr.DestroyBuffer(g.Device, g.VertexBuffer)
	g.vr.FreeMemory(g.Device, g.VertexBufferMemory)
	for i := 0; i < int(g.FramesInFlightCount); i++ {
		if len(g.RenderFinishedSemaphores) > 0 && len(g.RenderFinishedSemaphores) > i {
			g.vr.DestroySemaphore(g.Device, g.RenderFinishedSemaphores[i])
		}
		if len(g.ImageAvailableSemaphores) > 0 && len(g.ImageAvailableSemaphores) > i {
			g.vr.DestroySemaphore(g.Device, g.ImageAvailableSemaphores[i])
		}
		if len(g.InFlightFences) > 0 && len(g.InFlightFences) > i {
			g.vr.DestroyFence(g.Device, g.InFlightFences[i])
		}
	}

	if g.CommandPool != nil {
		g.vr.DestroyCommandPool(g.Device, g.CommandPool)
	}

	if g.Device != nil {
		g.vr.DestroyDevice(g.Device)
	}
	if g.Surface != nil {
		g.vr.DestroySurface(g.Instance, g.Surface)
	}
	if g.Instance != nil {
		g.vr.DestroyInstance(g.Instance)
	}
}

func (g *GContext) PhysicalDevices() (properties []vk.PhysicalDeviceProperties, err error) {
	physicalDevices, err := g.vr.EnumeratePhysicalDevices(g.Instance)
	if err != nil {
		return nil, err
	}
	if len(physicalDevices) > 0 {
		for _, pd := range physicalDevices {
			properties = append(properties, g.vr.GetPhysicalDeviceProperties(pd))
		}
		return
	}
	return nil, nil
}

func (g *GContext) GetSwapChainSupportDetails() (SwapChainSupportDetails, error) {
	capabilities, err := g.vr.GetPhysicalDeviceSurfaceCapabilities(g.PhysicalDevice, g.Surface)
	if err != nil {
		return SwapChainSupportDetails{}, err
	}
	formats, err := g.vr.GetPhysicalDeviceSurfaceFormats(g.PhysicalDevice, g.Surface)
	if err != nil {
		return SwapChainSupportDetails{}, err
	}
	presentModes, err := g.vr.GetPhysicalDeviceSurfacePresentModes(g.PhysicalDevice, g.Surface)
	if err != nil {
		return SwapChainSupportDetails{}, err
	}
	return SwapChainSupportDetails{
		Capabilities: capabilities,
		Formats:      formats,
		PresentModes: presentModes,
	}, nil
}

func (g *GContext) PickPhysicalDevice() error {
	physicalDevices, err := g.vr.EnumeratePhysicalDevices(g.Instance)
	if err != nil {
		return err
	}
	if len(physicalDevices) > 0 {
		for _, pd := range physicalDevices {
			gf, pf, err := g.FindQueueFamilies(pd)
			if err != nil {
				return err
			}
			properties, err := g.vr.EnumerateDeviceExtensionProperties(pd, "")
			if err != nil {
				return err
			}
			found := 0
		loop:
			for _, p := range properties {
				for _, r := range deviceExtensions {
					if p.ExtensionName == r {
						found++
						if found == len(deviceExtensions) {
							break loop
						}
						continue loop
					}
				}
			}
			if len(gf) > 0 && len(pf) > 0 && found == len(deviceExtensions) {
				g.PhysicalDevice = pd
				return nil
			}
		}
	}
	return ErrorNoPhysicalDevices
}

func (g *GContext) FindQueueFamilies(physicalDevice vk.PhysicalDevice) (graphicsFamily []int, presentFamily []int, err error) {
	queueFamilies, err := g.vr.GetPhysicalDeviceQueueFamilyProperties(physicalDevice)
	if err != nil {
		return
	}

	for i, qf := range queueFamilies {
		if len(graphicsFamily) == 0 && (vk.QueueFlagBits(qf.QueueFlags)&vk.QueueGraphicsBit != 0) {
			graphicsFamily = append(graphicsFamily, i)
		}
		if len(presentFamily) == 0 {
			supported, err := g.vr.GetPhysicalDeviceSurfaceSupport(physicalDevice, uint32(i), g.Surface)
			if err != nil {
				return nil, nil, err
			}
			if supported {
				presentFamily = append(presentFamily, i)
			}
		}
		if len(graphicsFamily) > 0 && len(presentFamily) > 0 {
			return
		}

	}

	return nil, nil, ErrorNoUsableQueueFamilies
}

func uniqueQueueFamilies(l1 []int, l2 []int) (result []int) {
	in := append(l1, l2...)
loop1:
	for _, e1 := range in {
		for _, e2 := range result {
			if e1 == e2 {
				continue loop1
			}
		}
		result = append(result, e1)
	}
	return
}

func (g *GContext) CreateLogicalDevice() error {
	gf, pf, err := g.FindQueueFamilies(g.PhysicalDevice)
	if err != nil {
		return err
	}

	uniqueFamilies := uniqueQueueFamilies(gf, pf)

	var queueCreateInfos []vk.DeviceQueueCreateInfo
	for _, qf := range uniqueFamilies {
		queueCreateInfos = append(queueCreateInfos, vk.DeviceQueueCreateInfo{
			QueueFamilyIndex: uint32(qf),
			QueuePriorities:  []float32{1.0},
		})
	}

	createInfo := vk.DeviceCreateInfo{
		QueueCreateInfos:      queueCreateInfos,
		EnabledFeatures:       []vk.PhysicalDeviceFeatures{{}},
		EnabledExtensionNames: deviceExtensions,
	}

	if len(validationLayers) > 0 {
		createInfo.EnabledLayerNames = validationLayers
	}

	d, err := g.vr.CreateDevice(g.PhysicalDevice, &createInfo)
	if err != nil {
		return err
	}
	g.Device = d

	g.GraphicsQueue = g.vr.GetDeviceQueue(d, uint32(gf[0]), 0)
	g.PresentQueue = g.vr.GetDeviceQueue(d, uint32(pf[0]), 0)
	return nil
}

func (g *GContext) CreateSwapChain(window *glfw.Window) error {
	swapChainSupport, err := g.GetSwapChainSupportDetails()
	if err != nil {
		return err
	}

	surfaceFormat := swapChainSupport.ChooseSwapSurfaceFormat()
	presentMode := swapChainSupport.ChooseSwapPresentMode()
	extent := swapChainSupport.ChooseSwapExtent(window)

	imageCount := swapChainSupport.Capabilities.MinImageCount + 1
	if swapChainSupport.Capabilities.MaxImageCount > 0 && imageCount > swapChainSupport.Capabilities.MaxImageCount {
		imageCount = swapChainSupport.Capabilities.MaxImageCount
	}

	g.FramesInFlightCount = uint(imageCount + 1)

	createInfo := vk.SwapchainCreateInfo{
		Surface:          g.Surface,
		MinImageCount:    imageCount,
		ImageFormat:      surfaceFormat.Format,
		ImageColorSpace:  surfaceFormat.ColorSpace,
		ImageExtent:      extent,
		ImageArrayLayers: 1,
		ImageUsage:       vk.ImageUsageFlags(vk.ImageUsageColorAttachmentBit),
	}

	gf, pf, err := g.FindQueueFamilies(g.PhysicalDevice)
	if err != nil {
		return err
	}
	queueFamilyIndices := []uint32{uint32(gf[0]), uint32(pf[0])}

	if gf[0] != pf[0] {
		createInfo.ImageSharingMode = vk.SharingModeConcurrent
		createInfo.QueueFamilyIndexCount = 2
		createInfo.QueueFamilyIndices = queueFamilyIndices
	} else {
		createInfo.ImageSharingMode = vk.SharingModeExclusive
	}

	createInfo.PreTransform = swapChainSupport.Capabilities.CurrentTransform
	createInfo.CompositeAlpha = vk.CompositeAlphaOpaqueBit
	createInfo.PresentMode = presentMode
	createInfo.Clipped = true
	createInfo.OldSwapchain = nil

	swapChain, err := g.vr.CreateSwapchain(g.Device, &createInfo)
	if err != nil {
		return err
	}
	g.SwapChain = swapChain

	i, err := g.vr.GetSwapchainImages(g.Device, swapChain)
	if err != nil {
		return err
	}
	g.SwapChainImages = i
	g.SwapChainImageFormat = surfaceFormat.Format
	g.SwapChainExtent = extent
	return nil
}

func (g *GContext) CreateSwapChainImageViews() error {
	for _, si := range g.SwapChainImages {
		createInfo := vk.ImageViewCreateInfo{
			Image:    si,
			ViewType: vk.ImageViewType2d,
			Format:   g.SwapChainImageFormat,
			Components: vk.ComponentMapping{
				R: vk.ComponentSwizzleIdentity,
				G: vk.ComponentSwizzleIdentity,
				B: vk.ComponentSwizzleIdentity,
				A: vk.ComponentSwizzleIdentity,
			},
			SubresourceRange: vk.ImageSubresourceRange{
				AspectMask:     vk.ImageAspectFlags(vk.ImageAspectColorBit),
				BaseMipLevel:   0,
				LevelCount:     1,
				BaseArrayLayer: 0,
				LayerCount:     1,
			},
		}
		sciv, err := g.vr.CreateImageView(g.Device, &createInfo)
		if err != nil {
			return err
		}
		g.SwapChainImageViews = append(g.SwapChainImageViews, sciv)
	}

	return nil
}

func (g *GContext) CreateGraphicsPipeline(vert []byte, frag []byte) error {
	vertModule, err := g.CreateShaderModule(vert)
	if err != nil {
		return err
	}
	fragModule, err := g.CreateShaderModule(frag)
	if err != nil {
		return err
	}

	vertShaderStageInfo := vk.PipelineShaderStageCreateInfo{
		Stage:  vk.ShaderStageVertexBit,
		Module: vertModule,
		Name:   "main",
	}

	fragShaderStageInfo := vk.PipelineShaderStageCreateInfo{
		Stage:  vk.ShaderStageFragmentBit,
		Module: fragModule,
		Name:   "main",
	}

	shaderStages := []vk.PipelineShaderStageCreateInfo{vertShaderStageInfo, fragShaderStageInfo}

	bindingDescription := getBindingDescription()
	attributeDescriptions := getAttributeDescriptions()

	vertexInputInfo := vk.PipelineVertexInputStateCreateInfo{
		VertexBindingDescriptions: []vk.VertexInputBindingDescription{bindingDescription},
		VertexAttributeDescriptions: attributeDescriptions,
	}

	inputAssembly := vk.PipelineInputAssemblyStateCreateInfo{
		Topology: vk.PrimitiveTopologyTriangleList,
	}

	viewPort := vk.Viewport{
		Width:    float32(g.SwapChainExtent.Width),
		Height:   float32(g.SwapChainExtent.Height),
		MaxDepth: 1.0,
	}

	scissor := vk.Rect2D{
		Extent: g.SwapChainExtent,
	}

	viewportState := vk.PipelineViewportStateCreateInfo{
		Viewports: []vk.Viewport{viewPort},
		Scissors:  []vk.Rect2D{scissor},
	}

	rasterizer := vk.PipelineRasterizationStateCreateInfo{
		PolygonMode: vk.PolygonModeFill,
		CullMode:    vk.CullModeFlags(vk.CullModeBackBit),
		FrontFace:   vk.FrontFaceClockwise,
		LineWidth:   1.0,
	}

	multisampling := vk.PipelineMultisampleStateCreateInfo{
		RasterizationSamples: vk.SampleCount1Bit,
	}

	colorBlendAttachment := vk.PipelineColorBlendAttachmentState{
		ColorWriteMask: vk.ColorComponentFlags(vk.ColorComponentRBit | vk.ColorComponentGBit | vk.ColorComponentBBit | vk.ColorComponentABit),
	}

	colorBlending := vk.PipelineColorBlendStateCreateInfo{
		LogicOp:     vk.LogicOpCopy,
		Attachments: []vk.PipelineColorBlendAttachmentState{colorBlendAttachment},
	}

	pipelineLayoutInfo := vk.PipelineLayoutCreateInfo{}

	pipelineLayout, err := g.vr.CreatePipelineLayout(g.Device, &pipelineLayoutInfo)
	if err != nil {
		return err
	}

	g.PipelineLayout = pipelineLayout

	pipelineInfo := vk.GraphicsPipelineCreateInfo{
		Stages:             shaderStages,
		VertexInputState:   &vertexInputInfo,
		InputAssemblyState: &inputAssembly,
		ViewportState:      &viewportState,
		RasterizationState: &rasterizer,
		MultisampleState:   &multisampling,
		ColorBlendState:    &colorBlending,
		Layout:             g.PipelineLayout,
		RenderPass:         g.RenderPass,
	}

	ps, err := g.vr.CreateGraphicsPipelines(g.Device, nil, []vk.GraphicsPipelineCreateInfo{pipelineInfo})
	if err != nil {
		return err
	}
	g.GraphicsPipeline = ps[0]

	g.vr.DestroyShaderModule(g.Device, fragModule)
	g.vr.DestroyShaderModule(g.Device, vertModule)
	return nil
}

func (g *GContext) CreateShaderModule(code []byte) (vk.ShaderModule, error) {
	createInfo := vk.ShaderModuleCreateInfo{
		Code: code,
	}
	return g.vr.CreateShaderModule(g.Device, &createInfo)
}

func (g *GContext) CreateRenderPass() error {
	colorAttachment := vk.AttachmentDescription{
		Format:         g.SwapChainImageFormat,
		Samples:        vk.SampleCount1Bit,
		LoadOp:         vk.AttachmentLoadOpClear,
		StoreOp:        vk.AttachmentStoreOpStore,
		StencilLoadOp:  vk.AttachmentLoadOpDontCare,
		StencilStoreOp: vk.AttachmentStoreOpDontCare,
		InitialLayout:  vk.ImageLayoutUndefined,
		FinalLayout:    vk.ImageLayoutPresentSrc,
	}

	colorAttachmentRef := vk.AttachmentReference{
		Layout: vk.ImageLayoutColorAttachmentOptimal,
	}

	subpass := vk.SubpassDescription{
		PipelineBindPoint: vk.PipelineBindPointGraphics,
		ColorAttachments:  []vk.AttachmentReference{colorAttachmentRef},
	}

	dependency := vk.SubpassDependency{
		SrcSubpass:    vk.SubpassExternal,
		SrcStageMask:  vk.PipelineStageFlags(vk.PipelineStageColorAttachmentOutputBit),
		DstStageMask:  vk.PipelineStageFlags(vk.PipelineStageColorAttachmentOutputBit),
		DstAccessMask: vk.AccessFlags(vk.AccessColorAttachmentWriteBit),
	}

	renderPassInfo := vk.RenderPassCreateInfo{
		Attachments:  []vk.AttachmentDescription{colorAttachment},
		Subpasses:    []vk.SubpassDescription{subpass},
		Dependencies: []vk.SubpassDependency{dependency},
	}

	rp, err := g.vr.CreateRenderPass(g.Device, &renderPassInfo)
	if err != nil {
		return err
	}

	g.RenderPass = rp

	return nil
}

func (g *GContext) CreateFramebuffers() error {
	var fbs []vk.Framebuffer

	for _, sciv := range g.SwapChainImageViews {
		attachements := []vk.ImageView{sciv}
		framebufferInfo := vk.FramebufferCreateInfo{
			RenderPass:  g.RenderPass,
			Attachments: attachements,
			Width:       g.SwapChainExtent.Width,
			Height:      g.SwapChainExtent.Height,
			Layers:      1,
		}

		fb, err := g.vr.CreateFramebuffer(g.Device, &framebufferInfo)
		if err != nil {
			for _, f := range fbs {
				g.vr.DestroyFramebuffer(g.Device, f)
			}
			return err
		}
		fbs = append(fbs, fb)
	}

	g.SwapChainFramebuffers = fbs

	return nil
}

func (g *GContext) CreateCommandPool() error {
	gf, _, err := g.FindQueueFamilies(g.PhysicalDevice)
	if err != nil {
		return err
	}

	commandPoolInfo := vk.CommandPoolCreateInfo{
		QueueFamilyIndex: uint32(gf[0]),
	}

	cp, err := g.vr.CreateCommandPool(g.Device, &commandPoolInfo)
	if err != nil {
		return err
	}

	g.CommandPool = cp

	return nil
}

func (g *GContext) CreateCommandBuffers() error {
	allocInfo := vk.CommandBufferAllocateInfo{
		CommandPool:        g.CommandPool,
		Level:              vk.CommandBufferLevelPrimary,
		CommandBufferCount: uint32(len(g.SwapChainFramebuffers)),
	}

	commandBuffers, err := g.vr.AllocateCommandBuffers(g.Device, &allocInfo)
	if err != nil {
		return err
	}

	g.CommandBuffers = commandBuffers

	for i, c := range commandBuffers {
		beginInfo := vk.CommandBufferBeginInfo{}
		err = g.vr.BeginCommandBuffer(c, &beginInfo)
		if err != nil {
			return err
		}

		clearColor := vk.NewClearValueFloats([4]float32{0.0, 0.0, 0.0, 1.0})
		renderPassInfo := vk.RenderPassBeginInfo{
			RenderPass:  g.RenderPass,
			Framebuffer: g.SwapChainFramebuffers[i],
			RenderArea:  vk.Rect2D{Extent: g.SwapChainExtent},
			ClearValues: []vk.ClearValue{clearColor},
		}

		g.vr.CmdBeginRenderPass(commandBuffers[i], &renderPassInfo, vk.SubpassContentsInline)
		g.vr.CmdBindPipeline(commandBuffers[i], vk.PipelineBindPointGraphics, g.GraphicsPipeline)
		vertexBuffers := [ ]vk.Buffer{g.VertexBuffer}
		offsets := []vk.DeviceSize{0}
		g.vr.CmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets)
		g.vr.CmdDraw(commandBuffers[i], uint32(len(vertices)), 1, 0, 0)
		g.vr.CmdEndRenderPass(commandBuffers[i])

		err = g.vr.EndCommandBuffer(commandBuffers[i])
		if err != nil {
			return err
		}
	}

	return nil
}

func (g *GContext) findMemoryType(typeFilter uint32, properties vk.MemoryPropertyFlags) (uint32, error) {
	memProperties := g.vr.GetPhysicalDeviceMemoryProperties(g.PhysicalDevice)

	for i, mt := range memProperties.MemoryTypes {
		if (typeFilter & (1 << i)) != 0 && (mt.PropertyFlags & properties) == properties {
			return uint32(i), nil
		}
 	}
	return 0, fmt.Errorf("error: no suitable memory type found")
}

func (g *GContext) CreateVertexBuffer() error {
	bufferInfo := vk.BufferCreateInfo{
		Size: vk.DeviceSize(int(unsafe.Sizeof(vertices[0])) * len(vertices)),
		Usage: vk.BufferUsageFlags(vk.BufferUsageVertexBufferBit),
		SharingMode: vk.SharingModeExclusive,
	}
	vb, err := g.vr.CreateBuffer(g.Device, &bufferInfo)
	if err != nil {
		return err
	}
	g.VertexBuffer = vb

	memRequirements := g.vr.GetBufferMemoryRequirements(g.Device, g.VertexBuffer)

	mti, err := g.findMemoryType(memRequirements.MemoryTypeBits, vk.MemoryPropertyFlags(vk.MemoryPropertyHostVisibleBit | vk.MemoryPropertyHostCoherentBit))
	if err != nil {
		return err
	}

	allocInfo := vk.MemoryAllocateInfo{
		AllocationSize:  memRequirements.Size,
		MemoryTypeIndex: mti,
	}

	vbm, err := g.vr.AllocateMemory(g.Device, &allocInfo)
	if err != nil {
		return err
	}

	g.VertexBufferMemory = vbm

	err = g.vr.BindBufferMemory(g.Device, g.VertexBuffer, g.VertexBufferMemory, 0)
	if err != nil {
		return err
	}

	ptr, err := g.vr.MapMemory(g.Device, g.VertexBufferMemory, 0, bufferInfo.Size, 0)
	if err != nil {
		return err
	}

	vk.MemNcopy(ptr, unsafe.Pointer(&vertices[0]), int(unsafe.Sizeof(vertices)) * len(vertices))
	g.vr.UnmapMemory(g.Device, g.VertexBufferMemory)

	return nil
}

func (g *GContext) CreateSyncObjects() error {
	g.ImagesInFlight = make([]vk.Fence, g.FramesInFlightCount)
	semaphoreInfo := vk.SemaphoreCreateInfo{}
	fenceInfo := vk.FenceCreateInfo{
		Flags: vk.FenceCreateFlags(vk.FenceCreateSignaledBit),
	}

	for i := 0; i < int(g.FramesInFlightCount); i++ {
		is, err := g.vr.CreateSemaphore(g.Device, &semaphoreInfo)
		if err != nil {
			return err
		}
		g.ImageAvailableSemaphores = append(g.ImageAvailableSemaphores, is)

		rs, err := g.vr.CreateSemaphore(g.Device, &semaphoreInfo)
		if err != nil {
			return err
		}
		g.RenderFinishedSemaphores = append(g.RenderFinishedSemaphores, rs)

		fence, err := g.vr.CreateFence(g.Device, &fenceInfo)
		if err != nil {
			return err
		}
		g.InFlightFences = append(g.InFlightFences, fence)
	}

	return nil
}

func (g *GContext) DeviceWait() error {
	return g.vr.DeviceWaitIdle(g.Device)
}

func (g *GContext) DrawFrame(window *glfw.Window,
	vert []byte,
	frag []byte) error {
	err := g.vr.WaitForFences(g.Device, []vk.Fence{g.InFlightFences[g.CurrentFrame]}, true, math.MaxUint64)
	if err != nil {
		return err
	}

	imageIndex, err := g.vr.AcquireNextImage(g.Device, g.SwapChain, math.MaxUint64, g.ImageAvailableSemaphores[g.CurrentFrame], nil)
	if err == vk.ErrErrorOutOfDate {
		return g.RecreateSwapChain(window, vert, frag)
	} else if err != nil {
		return err
	}

	if g.ImagesInFlight[imageIndex] != nil {
		err = g.vr.WaitForFences(g.Device, []vk.Fence{g.ImagesInFlight[imageIndex]}, true, math.MaxUint64)
		if err != nil {
			return err
		}
	}
	g.ImagesInFlight[imageIndex] = g.InFlightFences[g.CurrentFrame]

	submitInfo := vk.SubmitInfo{
		WaitSemaphores:   []vk.Semaphore{g.ImageAvailableSemaphores[g.CurrentFrame]},
		WaitDstStageMask: []vk.PipelineStageFlags{vk.PipelineStageFlags(vk.PipelineStageColorAttachmentOutputBit)},
		CommandBuffers:   []vk.CommandBuffer{g.CommandBuffers[imageIndex]},
		SignalSemaphores: []vk.Semaphore{g.RenderFinishedSemaphores[g.CurrentFrame]},
	}

	err = g.vr.ResetFences(g.Device, []vk.Fence{g.InFlightFences[g.CurrentFrame]})
	if err != nil {
		return err
	}

	err = g.vr.QueueSubmit(g.GraphicsQueue, []vk.SubmitInfo{submitInfo}, g.InFlightFences[g.CurrentFrame])
	if err != nil {
		return err
	}

	presentInfo := vk.PresentInfo{
		WaitSemaphores: []vk.Semaphore{g.RenderFinishedSemaphores[g.CurrentFrame]},
		Swapchains:     []vk.Swapchain{g.SwapChain},
		ImageIndices:   []uint32{imageIndex},
	}

	err = g.vr.QueuePresent(g.GraphicsQueue, &presentInfo)
	if err == vk.ErrErrorOutOfDate || err == vk.ErrSuboptimal || g.FramebufferResized {
		g.FramebufferResized = false
		err = g.RecreateSwapChain(window, vert, frag)
		if err != nil {
			return err
		}
	}
	g.CurrentFrame = (g.CurrentFrame + 1) % uint32(g.FramesInFlightCount)
	return err
}
