package graphics

import "C"
import (
	"fmt"

	vk "gitlab.com/corruptmemory/go-vulkan"
)

var (
	extensions = []string{
		vk.KhrSurfaceExtensionName,
		vk.KhrGetPhysicalDeviceProperties2ExtensionName,
		vk.KhrGetSurfaceCapabilities2ExtensionName,
	}

	deviceExtensions = []string{
		vk.KhrSwapchainExtensionName,
	}
)

var (
	ApiVersion11 = vk.MakeVersion(1, 1, 0)
	ApiVersion12 = vk.MakeVersion(1, 2, 0)
)

func checkVulkanExtensionSupport(vr vk.VulkanResource) error {
	properties, err :=  vr.EnumerateInstanceExtensionProperties("")
	if err != nil {
		return err
	}

	for _, p := range properties {
		fmt.Printf("extension: %s\n", p.ExtensionName)
	}

	remaining := len(extensions)
loop:
	for _, extensionName := range extensions {
		if remaining == 0 {
			return nil
		}
		for _, layerProperties := range properties {
			if extensionName == layerProperties.ExtensionName {
				remaining--
				continue loop
			}
		}
		return fmt.Errorf("error: could not find extension layer: '%s'", extensionName)
	}
	return nil
}

