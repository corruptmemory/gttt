module gitlab.com/corruptmemory/gttt

go 1.16

replace (
	gitlab.com/corruptmemory/go-vulkan => ../go-vulkan
	gitlab.com/corruptmemory/wavefrontmodel => ../wavefrontmodel
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb
	github.com/pkg/profile v1.6.0
	github.com/xlab/linmath v0.0.0-20170502193301-512668b827be
	gitlab.com/corruptmemory/go-vulkan v0.1.0
)
